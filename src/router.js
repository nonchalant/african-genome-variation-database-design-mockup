import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Gene from './views/Gene.vue'
import Api from './views/Api.vue'
import Variant from './views/Variant.vue'
import Contact from './views/Contact.vue'
import About from './views/About.vue'
import Tutorial from './views/Tutorial.vue'
import PrecisionMedicinePortal from './views/PrecisionMedicinePortal.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/gene',
      name: 'gene',
      component: Gene
    },
    {
      path: '/variant/:selection',
      name: 'variant',
      component: Variant
    },
    {
      path: '/contact',
      name: 'contact',
      component: Contact
    },
    {
      path: '/about',
      name: 'about',
      component: About
    },
    {
      path: '/tutorial',
      name: 'tutorial',
      component: Tutorial
    },
    {
      path: '/precision-medicine',
      name: 'precision-medicine',
      component: PrecisionMedicinePortal
    },
    {
      path: '/api',
      name: 'api',
      component: Api
    }
  ]
})

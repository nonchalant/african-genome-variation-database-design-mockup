import Vue from 'vue'
import Vuex from 'vuex'
import router from '@/router'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    search_selection: '',
    gene_selected: {
      name: ''
    },
    variant_selected: {
      name: ''
    }
  },
  mutations: {
    setSearchSelection (state, selection) {
      state.search_selection = selection
    }
  },
  actions: {
    newSearch (context, selection) {
      context.commit('setSearchSelection', selection)
      router.push({ name: 'variant', params: { selection } })
    }
  }
})

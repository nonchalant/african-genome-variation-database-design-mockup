# African Genome Variation Database

The African Genome Variation Database (AGVD) is a platform for exploring genomic variation data within African populations. 
This project forms part of the [H3ABioNet](https://www.h3abionet.org/tools-and-services/workflows) consortium.
Further information about the usage as well as scientific application of this platform can be found in the accompanying wiki, once it becomes available.
The remainder of this README is specifically focused toward developers and/or potential contributors to the platform.

## Application Structure

The application uses Flask & Flask-RestPlus to create a minimal REST style API,
and lets VueJs + vue-cli handle the front end and asset pipeline.
Data from the python server to the Vue application is passed by making Ajax requests.

![Vue Logo](docs/vue-logo.png "Vue Logo") ![Flask Logo](docs/flask-logo.png "Flask Logo")

### Application Stack
* Flask 1.0 to serve the backend
* [Flask-RestPlus](http://flask-restplus.readthedocs.io) API with class-based secure resource routing
* [MongoDB](https://www.mongodb.com/) as a NoSQL database to manage document assets
* [PyTest](http://pytest.org) test suite
* [vue-cli 3](https://github.com/vuejs/vue-cli/blob/dev/docs/README.md) + yarn
* [Vuex](https://vuex.vuejs.org/)
* [Vue Router](https://router.vuejs.org/)
* [Axios](https://vuex.vuejs.org/) for backend communication
* Heroku Configuration with one-click deployment + Gunicorn

### Rest Api

The Api is served using a Flask blueprint at `/api/` using Flask RestPlus class-based
resource routing.

### Client Application

A Flask view is used to serve the `index.html` as an entry point into the Vue app at the endpoint `/`.

Vue Cli & Webpack manage front-end resources and assets, so they overwrite template delimiter.


### Important Files

| Location             |  Content                                   |
|----------------------|--------------------------------------------|
| `/app`               | Flask Application                          |
| `/app/api`           | Flask Rest Api (`/api`)                    |
| `/app/client.py`     | Flask Client (`/`)                         |
| `/src`               | Vue App .                                  |
| `/src/main.js`       | JS Application Entry Point                 |
| `/public/index.html` | Html Application Entry Point (`/`)         |
| `/public/static`     | Static Assets                              |
| `/dist/`             | Bundled Assets Output (generated at `yarn build` |


## Installation

### Prerequisites

Before getting started, you should have the following installed and running:

- [X] Yarn - [instructions](https://yarnpkg.com/en/docs/install#mac-stable)
- [X] Vue Cli 3 - [instructions](https://cli.vuejs.org/guide/installation.html)
- [X] MongoDB
- [X] Python 3
- [X] Pipenv or conda (optional)
- [X] Heroku Cli (if deploying to Heroku)

### Template and Dependencies

* Clone this repository:

	```
	$ git clone https://gitlab.com/nonchalant/african-genome-variation-database.git
	```

* Setup virtual environment, install dependencies, and activate it:

	```
	$ pipenv install --dev
	$ pipenv shell
	$ mkdir ./dist
	```

* Install JS dependencies

	```
	$ yarn install
	```

## Development Server

Run Flask Api development server:

```
$ python run.py
```

From another tab in the same directory, start the webpack dev server:

```
$ yarn serve
```

The Vuejs application will be served from `localhost:8080` and the Flask Api
and static files will be served from `localhost:5000`.

The dual dev-server setup allows you to take advantage of
webpack's development server with hot module replacement.

Proxy config in `vue.config.js` is used to route the requests
back to Flask's Api on port 5000.

If you would rather run a single dev server, you can run Flask's
development server only on `:5000`, but you have to build the Vue app first
and the page will not reload on changes.

```
$ yarn build
$ python run.py
```

## Production Server

This application is configured to work with Heroku + Gunicorn and it's pre-configured
to have Heroku build the application before releasing it.

### JS Build Process

Heroku's nodejs buildpack will handle install for all the dependencies from the `packages.json` file.
It will then trigger the `postinstall` command which calls `yarn build`.
This will create the bundled `dist` folder which will be served by whitenoise.

### Python Build Process

The python buildpack will detect the `Pipfile` and install all the python dependencies.

### Production Sever Setup

Here are the commands we need to run to get things setup on the Heroku side:

	```
	$ heroku apps:create african-genome-variation-database
	$ heroku git:remote --app african-genome-variation-database
	$ heroku buildpacks:add --index 1 heroku/nodejs
	$ heroku buildpacks:add --index 2 heroku/python
	$ heroku config:set FLASK_ENV=production
	$ heroku config:set FLASK_SECRET=SuperSecretKey

	$ git push heroku
	```

### Heroku deployment - One Click Deploy

[![Deploy](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy?template=https://gitlab.com/nonchalant/african-genome-variation-database)

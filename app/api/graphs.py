import json

import altair as alt
import pandas as pd


def create_bar_plot(data, x=None, y=None, title=None, width=400, height=300, color='#38b6ff'):
    """
    Build altair bar plot specification using `data`
    """
    df = pd.DataFrame(data)

    bars = alt.Chart(df, width=width, height=height).mark_bar(color=color)
    bars = bars.encode(x=x, y=y)

    text = bars.mark_text(
        align='left',
        baseline='middle',
        dy=-10,  # Nudges text to right so it doesn't appear on top of the bar
        dx=-10,
    ).encode(
        text=f'{y}:Q'
    )

    plt_spec = (bars + text).properties(
        title=title
    )

    plt_spec = plt_spec.to_json()  # exports python chart into altair json text spec
    plt_spec = json.loads(plt_spec)  # converts json text string into js object

    return plt_spec

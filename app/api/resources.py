"""
REST API Resource Routing
http://flask-restplus.readthedocs.io
"""

from datetime import datetime
from flask import request
from flask_restplus import Resource

from app.api.graphs import create_bar_plot
from .security import require_auth
from . import api_rest


TEST_DATA = {
    "rs527413419": [
        {
          'sub-population': 'Yoruba',
          'allele_count': 8,
          'allele_total': 200,
          'minor_allele_frequency': 0.040
        },
        {
          'sub-population': 'Mbuti',
          'allele_count': 0,
          'allele_total': 67,
          'minor_allele_frequency': 0.000
        },
        {
          'sub-population': 'Mende',
          'allele_count': 2,
          'allele_total': 80,
          'minor_allele_frequency': 0.025
        },
        {
          'sub-population': 'Bantu',
          'allele_count': 28,
          'allele_total': 280,
          'minor_allele_frequency': 0.100
        }
      ]
}

class SecureResource(Resource):
    """ Calls require_auth decorator on all requests """
    method_decorators = [require_auth]


@api_rest.route('/resource/<string:resource_id>')
class ResourceOne(Resource):
    """ Unsecure Resource Class: Inherit from Resource """

    def get(self, resource_id):
        timestamp = datetime.utcnow().isoformat()
        return {'timestamp': timestamp}

    def post(self, resource_id):
        json_payload = request.json
        return {'timestamp': json_payload}, 201


@api_rest.route('/secure-resource/<string:resource_id>')
class SecureResourceOne(SecureResource):
    """ Unsecure Resource Class: Inherit from Resource """

    def get(self, resource_id):
        timestamp = datetime.utcnow().isoformat()
        return {'timestamp': timestamp}


@api_rest.route('/allele_freq_plot/<string:variant_name>')
class AlleleFreqPlot(Resource):
    """ Unsecure Resource Class: Inherit from Resource """

    def get(self, variant_name):
        try:
            data = TEST_DATA[variant_name]
            plt = create_bar_plot(data, x='sub-population', y='minor_allele_frequency', title=f"MAF for {variant_name} across African sub-populations")
        except KeyError:
            print("Variant, gene or co-ordinate not found in database")
            plt = {}
        return plt
